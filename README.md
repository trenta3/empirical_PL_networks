# Empirical Test of PL condition on Overparameterized Neural Networks
This repository tests whether a theory of overparameterized Neural Networks based on the Polyak-Łojasiewicz condition is a capable predictor of their behaviour.

## Quick Start
You need to have a computer with GPU and nvidia drivers, [conda](https://anaconda.org) and [dvc](https://dvc.org/) installed.

Just install the conda environment, which will install pytorch for GPU.
```bash
conda env create -p ./venv -f environment.yml
```

### Obtaining full experiments results
To simply obtain the results of the already performed tests it is enough to do a `dvc pull`, which will ask to authenticate with Google Drive (the folder is freely accessible, so any Google account will do), and the data will be found stored in `csv` files inside the folders `main/results` and `conditioning/results`.

The performed analysis can be found in the two jupytext produced versions of jupyter notebook `conditioning_analysis.py` and `general_analysis.py`.

### Reproducing experiments
Experiments of the paper can be reproduced exactly by issuing [`dvc repro`](https://dvc.org/doc/command-reference/repro).

### Producing other experiments
Experiment configurations can be found in the file `experiments.yml`, and additional experiments can be produced by:
```
./generate_experiments.py "FCN_STD_*MSE" --paramdir main/params/ --resultdir main/results/ > all_experiments.sh
CUDA_VISIBLE_DEVICES=x,y bash all_experiments.sh
```
